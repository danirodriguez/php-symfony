<?php
//2.creo el namespace
namespace App\Controller;
//1. creo la clase maincontroller

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;

class MainController extends AbstractController
{
    //creo la función para añadir anotaciones de la ruta, encima del metodo creo la ruta
    
    /**
     * @Route ("/maleteo" )
     */

    public function inicio(){
        return $this->render(
            "base.html.twig"
            );
    }
    /**
     * @Route ("/formulario" )
     */



    public function formulario(){
        return $this->render(
            "form.html.twig"

        );
    }


}