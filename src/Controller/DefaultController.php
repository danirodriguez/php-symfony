<?php
namespace App\Controller;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class DefaultController
{
    public function index(){
        return new Response("Hola mundo");
    }

    public function homepage(){
        return new Response("Esto es el HOMEPAGE");
    }

    /**
     * @Route("/contacto")
     */
    public function contacto(){
        return new Response("Contactoooooo");
    }

    /**
     * @Route("/usuario/{nombre}/{apellido}/edad/{edad}", methods={"GET", "POST"}, requirements={"edad"="\d+"})
     */
    public function usuario($nombre, $apellido, $edad){
        return new Response("
            <h1>El nombre es $nombre $apellido</h1>
            <h2>Y la edad es $edad</h2>
            ");
    }
}